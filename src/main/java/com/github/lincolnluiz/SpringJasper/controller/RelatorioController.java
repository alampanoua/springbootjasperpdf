package com.github.lincolnluiz.SpringJasper.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import net.sf.jasperreports.engine.util.JRLoader;

@Controller
@RequestMapping("/pdf")
public class RelatorioController {


	@PostMapping
	public @ResponseBody
	byte[] imprimir(HttpServletResponse response) throws JRException, IOException {
		Map<String, Object> parametros  = null;



	InputStream jasperStream = this.getClass().getResourceAsStream("/lifeapplicationnew.jasper");

	JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, new JREmptyDataSource(1));

	response.setContentType("application/pdf");
	response.setHeader("Content-Disposition", "attachment; filename=life.pdf");

	byte[] pdfBytes = JasperExportManager.exportReportToPdf(jasperPrint);

	return pdfBytes;

	}

}
