$(document).ready(function () {

    $("#search-form").submit(function (event) {
        console.log("start");
        $('.loading').css('display','block');
        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit(){
fileName = "life";
var req = new XMLHttpRequest();
req.open("POST", "/pdf" , true);
req.responseType = "blob";
fileName += "_"  + ".pdf";

req.onload = function (event) {

    var blob = req.response;

    //for IE
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, fileName);
    } else {

        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = fileName;
        link.click();
        console.log("final");
         $('.loading').css('display','none');
    }
};

req.send();
}

//function fire_ajax_submit() {
//
//    $("#search-form").prop("disabled", true);
//
//    $.ajax({
//        type: "POST",
////        contentType: "application/json",
//        url: "/relatorio",
//        contentType: 'arraybuffer',
//        cache: false,
//        timeout: 6000000,
//        success: function (data) {
//
//            console.log("SUCCESS : ");
//            $("#btn-search").prop("disabled", false);
//       // The actual download
//              var blob = new Blob([data], { type: 'application/pdf;base64' });
//              var link = document.createElement('a');
//              link.href = window.URL.createObjectURL(blob);
//
//              link.download = "life.pdf";
//
//              document.body.appendChild(link);
//              link.click();
//              document.body.removeChild(link);
//
//        },
//        error: function (e) {
//
//            console.log("ERROR : ", e);
//            $("#btn-search").prop("disabled", false);
//
//        }
//    });

//}